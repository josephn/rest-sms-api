package encoding

import (
	"encoding/hex"
	"reflect"
	"testing"
)

var testSet = []struct {
	UTF8Str     string
	ExpectedGSM []byte
}{
	{
		UTF8Str:     "",
		ExpectedGSM: []byte{},
	},
	{
		UTF8Str: "This is a string",
		ExpectedGSM: []byte{0x54, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20,
			0x61, 0x20, 0x73, 0x74, 0x72, 0x69, 0x6e, 0x67},
	},
	{
		UTF8Str: "{}|`!@#$%^&*()_+:;~<>?/\\",
		ExpectedGSM: []byte{0x1b, 0x28, 0x1b, 0x29, 0x1b, 0x40, 0x21, 0x00,
			0x23, 0x02, 0x25, 0x1b, 0x14, 0x26, 0x2a, 0x28,
			0x29, 0x11, 0x2b, 0x3a, 0x3b, 0x1b, 0x3d, 0x3c,
			0x3e, 0x3f, 0x2f, 0x1b, 0x2f},
	},
	{
		// This is an example of an UTF8 subset not covered bu the conversion.
		UTF8Str:     "他们说中文很容易",
		ExpectedGSM: []byte{},
	},
}

func TestUTFToGSMConversion(t *testing.T) {
	for iter, test := range testSet {
		gsmBytes := UTF8ToGSM(test.UTF8Str)

		if !reflect.DeepEqual(gsmBytes, test.ExpectedGSM) && len(gsmBytes)+len(test.ExpectedGSM) != 0 {
			t.Fatalf("Conversion from UTF8 to GSM failed at iter %d. \nexp: %s \ngot: %s",
				iter, hex.Dump(test.ExpectedGSM), hex.Dump(gsmBytes))
		}
	}
}
