package encoding

import "bytes"

// This UTF8 to GSM338 conversion table was extracted from the following references:
// - https://github.com/livebg/smstools/blob/master/lib/sms_tools/gsm_encoding.rb
// - https://en.wikipedia.org/wiki/GSM_03.38

// UTF8ToGSMTable includes the mapping from a UTF-8 charpoint to a GSM03.38 equivalent.
var UTF8ToGSMTable = map[rune]([]byte){
	0x0040: []byte{0x00}, // COMMERCIAL AT
	0x00A3: []byte{0x01}, // POUND SIGN
	0x0024: []byte{0x02}, // DOLLAR SIGN
	0x00A5: []byte{0x03}, // YEN SIGN
	0x00E8: []byte{0x04}, // LATIN SMALL LETTER E WITH GRAVE
	0x00E9: []byte{0x05}, // LATIN SMALL LETTER E WITH ACUTE
	0x00F9: []byte{0x06}, // LATIN SMALL LETTER U WITH GRAVE
	0x00EC: []byte{0x07}, // LATIN SMALL LETTER I WITH GRAVE
	0x00F2: []byte{0x08}, // LATIN SMALL LETTER O WITH GRAVE
	0x00C7: []byte{0x09}, // LATIN CAPITAL LETTER C WITH CEDILLA
	0x000A: []byte{0x0A}, // LINE FEED
	0x00D8: []byte{0x0B}, // LATIN CAPITAL LETTER O WITH STROKE
	0x00F8: []byte{0x0C}, // LATIN SMALL LETTER O WITH STROKE
	0x000D: []byte{0x0D}, // CARRIAGE RETURN
	0x00C5: []byte{0x0E}, // LATIN CAPITAL LETTER A WITH RING ABOVE
	0x00E5: []byte{0x0F}, // LATIN SMALL LETTER A WITH RING ABOVE
	0x0394: []byte{0x10}, // GREEK CAPITAL LETTER DELTA
	0x005F: []byte{0x11}, // LOW LINE
	0x03A6: []byte{0x12}, // GREEK CAPITAL LETTER PHI
	0x0393: []byte{0x13}, // GREEK CAPITAL LETTER GAMMA
	0x039B: []byte{0x14}, // GREEK CAPITAL LETTER LAMDA
	0x03A9: []byte{0x15}, // GREEK CAPITAL LETTER OMEGA
	0x03A0: []byte{0x16}, // GREEK CAPITAL LETTER PI
	0x03A8: []byte{0x17}, // GREEK CAPITAL LETTER PSI
	0x03A3: []byte{0x18}, // GREEK CAPITAL LETTER SIGMA
	0x0398: []byte{0x19}, // GREEK CAPITAL LETTER THETA
	0x039E: []byte{0x1A}, // GREEK CAPITAL LETTER XI
	0x00C6: []byte{0x1C}, // LATIN CAPITAL LETTER AE
	0x00E6: []byte{0x1D}, // LATIN SMALL LETTER AE
	0x00DF: []byte{0x1E}, // LATIN SMALL LETTER SHARP S (German)
	0x00C9: []byte{0x1F}, // LATIN CAPITAL LETTER E WITH ACUTE
	0x0020: []byte{0x20}, // SPACE
	0x0021: []byte{0x21}, // EXCLAMATION MARK
	0x0022: []byte{0x22}, // QUOTATION MARK
	0x0023: []byte{0x23}, // NUMBER SIGN
	0x00A4: []byte{0x24}, // CURRENCY SIGN
	0x0025: []byte{0x25}, // PERCENT SIGN
	0x0026: []byte{0x26}, // AMPERSAND
	0x0027: []byte{0x27}, // APOSTROPHE
	0x0028: []byte{0x28}, // LEFT PARENTHESIS
	0x0029: []byte{0x29}, // RIGHT PARENTHESIS
	0x002A: []byte{0x2A}, // ASTERISK
	0x002B: []byte{0x2B}, // PLUS SIGN
	0x002C: []byte{0x2C}, // COMMA
	0x002D: []byte{0x2D}, // HYPHEN-MINUS
	0x002E: []byte{0x2E}, // FULL STOP
	0x002F: []byte{0x2F}, // SOLIDUS
	0x0030: []byte{0x30}, // DIGIT ZERO
	0x0031: []byte{0x31}, // DIGIT ONE
	0x0032: []byte{0x32}, // DIGIT TWO
	0x0033: []byte{0x33}, // DIGIT THREE
	0x0034: []byte{0x34}, // DIGIT FOUR
	0x0035: []byte{0x35}, // DIGIT FIVE
	0x0036: []byte{0x36}, // DIGIT SIX
	0x0037: []byte{0x37}, // DIGIT SEVEN
	0x0038: []byte{0x38}, // DIGIT EIGHT
	0x0039: []byte{0x39}, // DIGIT NINE
	0x003A: []byte{0x3A}, // COLON
	0x003B: []byte{0x3B}, // SEMICOLON
	0x003C: []byte{0x3C}, // LESS-THAN SIGN
	0x003D: []byte{0x3D}, // EQUALS SIGN
	0x003E: []byte{0x3E}, // GREATER-THAN SIGN
	0x003F: []byte{0x3F}, // QUESTION MARK
	0x00A1: []byte{0x40}, // INVERTED EXCLAMATION MARK
	0x0041: []byte{0x41}, // LATIN CAPITAL LETTER A
	0x0042: []byte{0x42}, // LATIN CAPITAL LETTER B
	0x0043: []byte{0x43}, // LATIN CAPITAL LETTER C
	0x0044: []byte{0x44}, // LATIN CAPITAL LETTER D
	0x0045: []byte{0x45}, // LATIN CAPITAL LETTER E
	0x0046: []byte{0x46}, // LATIN CAPITAL LETTER F
	0x0047: []byte{0x47}, // LATIN CAPITAL LETTER G
	0x0048: []byte{0x48}, // LATIN CAPITAL LETTER H
	0x0049: []byte{0x49}, // LATIN CAPITAL LETTER I
	0x004A: []byte{0x4A}, // LATIN CAPITAL LETTER J
	0x004B: []byte{0x4B}, // LATIN CAPITAL LETTER K
	0x004C: []byte{0x4C}, // LATIN CAPITAL LETTER L
	0x004D: []byte{0x4D}, // LATIN CAPITAL LETTER M
	0x004E: []byte{0x4E}, // LATIN CAPITAL LETTER N
	0x004F: []byte{0x4F}, // LATIN CAPITAL LETTER O
	0x0050: []byte{0x50}, // LATIN CAPITAL LETTER P
	0x0051: []byte{0x51}, // LATIN CAPITAL LETTER Q
	0x0052: []byte{0x52}, // LATIN CAPITAL LETTER R
	0x0053: []byte{0x53}, // LATIN CAPITAL LETTER S
	0x0054: []byte{0x54}, // LATIN CAPITAL LETTER T
	0x0055: []byte{0x55}, // LATIN CAPITAL LETTER U
	0x0056: []byte{0x56}, // LATIN CAPITAL LETTER V
	0x0057: []byte{0x57}, // LATIN CAPITAL LETTER W
	0x0058: []byte{0x58}, // LATIN CAPITAL LETTER X
	0x0059: []byte{0x59}, // LATIN CAPITAL LETTER Y
	0x005A: []byte{0x5A}, // LATIN CAPITAL LETTER Z
	0x00C4: []byte{0x5B}, // LATIN CAPITAL LETTER A WITH DIAERESIS
	0x00D6: []byte{0x5C}, // LATIN CAPITAL LETTER O WITH DIAERESIS
	0x00D1: []byte{0x5D}, // LATIN CAPITAL LETTER N WITH TILDE
	0x00DC: []byte{0x5E}, // LATIN CAPITAL LETTER U WITH DIAERESIS
	0x00A7: []byte{0x5F}, // SECTION SIGN
	0x00BF: []byte{0x60}, // INVERTED QUESTION MARK
	0x0061: []byte{0x61}, // LATIN SMALL LETTER A
	0x0062: []byte{0x62}, // LATIN SMALL LETTER B
	0x0063: []byte{0x63}, // LATIN SMALL LETTER C
	0x0064: []byte{0x64}, // LATIN SMALL LETTER D
	0x0065: []byte{0x65}, // LATIN SMALL LETTER E
	0x0066: []byte{0x66}, // LATIN SMALL LETTER F
	0x0067: []byte{0x67}, // LATIN SMALL LETTER G
	0x0068: []byte{0x68}, // LATIN SMALL LETTER H
	0x0069: []byte{0x69}, // LATIN SMALL LETTER I
	0x006A: []byte{0x6A}, // LATIN SMALL LETTER J
	0x006B: []byte{0x6B}, // LATIN SMALL LETTER K
	0x006C: []byte{0x6C}, // LATIN SMALL LETTER L
	0x006D: []byte{0x6D}, // LATIN SMALL LETTER M
	0x006E: []byte{0x6E}, // LATIN SMALL LETTER N
	0x006F: []byte{0x6F}, // LATIN SMALL LETTER O
	0x0070: []byte{0x70}, // LATIN SMALL LETTER P
	0x0071: []byte{0x71}, // LATIN SMALL LETTER Q
	0x0072: []byte{0x72}, // LATIN SMALL LETTER R
	0x0073: []byte{0x73}, // LATIN SMALL LETTER S
	0x0074: []byte{0x74}, // LATIN SMALL LETTER T
	0x0075: []byte{0x75}, // LATIN SMALL LETTER U
	0x0076: []byte{0x76}, // LATIN SMALL LETTER V
	0x0077: []byte{0x77}, // LATIN SMALL LETTER W
	0x0078: []byte{0x78}, // LATIN SMALL LETTER X
	0x0079: []byte{0x79}, // LATIN SMALL LETTER Y
	0x007A: []byte{0x7A}, // LATIN SMALL LETTER Z
	0x00E4: []byte{0x7B}, // LATIN SMALL LETTER A WITH DIAERESIS
	0x00F6: []byte{0x7C}, // LATIN SMALL LETTER O WITH DIAERESIS
	0x00F1: []byte{0x7D}, // LATIN SMALL LETTER N WITH TILDE
	0x00FC: []byte{0x7E}, // LATIN SMALL LETTER U WITH DIAERESIS
	0x00E0: []byte{0x7F}, // LATIN SMALL LETTER A WITH GRAVE
}

// UTF8ToGSMExtTable includes an extension to UTF8ToGSMExtTable
// with additional conversions.
var UTF8ToGSMExtTable = map[rune]([]byte){
	0x000C: []byte{0x1B, 0x0A}, // FORM FEED
	0x005E: []byte{0x1B, 0x14}, // CIRCUMFLEX ACCENT
	0x007B: []byte{0x1B, 0x28}, // LEFT CURLY BRACKET
	0x007D: []byte{0x1B, 0x29}, // RIGHT CURLY BRACKET
	0x005C: []byte{0x1B, 0x2F}, // REVERSE SOLIDUS
	0x005B: []byte{0x1B, 0x3C}, // LEFT SQUARE BRACKET
	0x007E: []byte{0x1B, 0x3D}, // TILDE
	0x005D: []byte{0x1B, 0x3E}, // RIGHT SQUARE BRACKET
	0x007C: []byte{0x1B, 0x40}, // VERTICAL LINE
	0x20AC: []byte{0x1B, 0x65}, // EURO SIGN
}

func ConvertUTF8ToGSM(r rune) ([]byte, int) {
	gsmStr, ok := UTF8ToGSMTable[r]
	if ok {
		return gsmStr, 1
	}

	gsmStr, ok = UTF8ToGSMExtTable[r]
	if ok {
		return gsmStr, 2
	}

	return []byte{}, 0
}

// UTF8ToGSM converts an UTF-8 string to GSM03.38 format.
func UTF8ToGSM(msg string) []byte {
	if len(msg) == 0 {
		return []byte{}
	}

	utf8Runes := []rune(msg)
	var buffer bytes.Buffer

	for _, utf8Rune := range utf8Runes {
		gsmStr, _ := ConvertUTF8ToGSM(utf8Rune)
		buffer.Write(gsmStr)
	}

	return buffer.Bytes()
}
