package sms

import (
	"bytes"
	"math/rand"
	"reflect"
	"testing"
	"time"

	"github.com/oscarpfernandez/rest-sms-api/encoding"
)

func TestSplitSms(t *testing.T) {
	randStrLenght := rand.Intn(1000) + 3000

	origMsg := &Message{
		FullText: generateRandString(randStrLenght),
		Encoding: "utf8",
	}

	sms, _ := origMsg.CreateConcatenatedSMS()

	cmsRefID := sms.CmsRefNumber
	buffer := bytes.Buffer{}

	for iter, smsBlock := range sms.SMSBlocks {
		buffer.Write(smsBlock.Message)

		if exp, got := SmsMaxPayloadBytes, len(smsBlock.Message); got > exp {
			t.Fatalf("SMS text payload size is larger than allowed. exp: %d, got: %d", exp, got)
		}
		if exp, got := cmsRefID, smsBlock.UDH[3]; exp != got {
			t.Fatalf("UDH refID does not match. exp: %d, got: %d", exp, got)
		}
		if exp, got := byte(iter+1), smsBlock.UDH[5]; exp != got {
			t.Fatalf("UDH SeqID does not match. exp: %d, got: %d", exp, got)
		}
	}

	// Let's verify if the original GSM7 converted message matches with the
	// re-assemebled message, to assert that data is not getting lost during
	// the split.
	origGsmBytes := encoding.UTF8ToGSM(origMsg.FullText)
	if exp, got := origGsmBytes, buffer.Bytes(); !reflect.DeepEqual(exp, got) {
		t.Fatalf("The original GSM7 text does not match with the concatenated text")
	}
}

func TestSplitSmsWithInvalidEncoding(t *testing.T) {
	origMsg := &Message{
		FullText: "the message",
		Encoding: "anythingButUTF8",
	}

	_, err := origMsg.CreateConcatenatedSMS()

	if e, ok := err.(error); !ok || e != ErrEncodingNotSupported {
		t.Fatalf("Error returned is not expected")
	}
}

func TestSaveSmsBlock(t *testing.T) {
	sms := Sms{}
	numSmsBlocks := 100

	var buffer bytes.Buffer
	for iter := 0; iter < numSmsBlocks; iter++ {
		randomBytes := make([]byte, 10, 10)
		rand.Read(randomBytes)
		buffer.Write(randomBytes)
		sms.saveSmsBlock(buffer.Bytes(), "originator", []string{"1234"})

		if exp, got := buffer.Bytes(), sms.SMSBlocks[len(sms.SMSBlocks)-1].Message; !reflect.DeepEqual(exp, got) {
			t.Fatalf("Saved sms block does not match. exp: %v, got: %v", exp, got)
		}
	}

	if exp, got := numSmsBlocks, len(sms.SMSBlocks); exp != got {
		t.Fatalf("Number sms blocks do not match. exp: %d, got: %d", exp, got)
	}
}

func TestSetUDHInSmsBlocks(t *testing.T) {
	numSmsBlocks := 100

	sms := &Sms{
		CmsRefNumber: (byte)(rand.Intn(255)),
	}

	for iter := 0; iter < numSmsBlocks; iter++ {
		var buffer bytes.Buffer
		sms.saveSmsBlock(buffer.Bytes(), "originator", []string{"1234"})
	}

	sms.setUdhInAllSmsBlocks()

	for iter, smsBlock := range sms.SMSBlocks {
		udh := smsBlock.UDH
		if exp, got := sms.CmsRefNumber, udh[3]; exp != got {
			t.Fatalf("UDH cms refID does not match, on iter: %d. exp: %d, got: %d", iter, exp, got)
		}

		if exp, got := byte(iter+1), udh[5]; exp != got {
			t.Fatalf("UDH seqID does not match, on iter: %d. exp: %d, got: %d", iter, exp, got)
		}
	}

}

func TestGenerateUDH(t *testing.T) {
	for iter := 0; iter < 100; iter++ {
		refID := (byte)(rand.Intn(255))
		seqID := (byte)(rand.Intn(255))
		totalNumSeqs := (byte)(rand.Intn(255))

		expUDH := []byte{0x05, 0x00, 0x03, refID, totalNumSeqs, seqID}
		gotUDH := generateUDH(refID, totalNumSeqs, seqID)

		if !reflect.DeepEqual(expUDH, gotUDH) {
			t.Fatalf("Generated UDH not generated correctly. exp: %v, got: %v", expUDH, gotUDH)
		}
	}
}

// generateRandString create a random text string, provided a maximum length.
// Half the string will be composed by utf8 chars that once converted to GSM7
// will have a single byte, while the other half is composed by characters
// that converted, will have two bytes. This is to force all the cases when
// generating the composed SMS.
func generateRandString(length int) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	const singleByteGsmChars = "abcdefghijklmnopqrstuvwxyz0123456789"
	const doubleByteGsmChars = "`[]{}|$%^€\\"

	bufferStr := bytes.NewBufferString("")

	halfSize := length / 2

	for i := 0; i < halfSize; i++ {
		if i%10 == 0 {
			bufferStr.WriteString(" ")
		} else {
			bufferStr.WriteByte(singleByteGsmChars[r.Intn(len(singleByteGsmChars))])
		}
	}

	for i := 0; i < length-halfSize; i++ {
		if i%10 == 0 {
			bufferStr.WriteString(" ")
		} else {
			bufferStr.WriteByte(doubleByteGsmChars[r.Intn(len(doubleByteGsmChars))])
		}
	}

	return bufferStr.String()
}

func BenchmarkSplitSms(b *testing.B) {

	randStrLenght := 400

	origMsg := &Message{
		FullText: generateRandString(randStrLenght),
		Encoding: "utf8",
	}

	for n := 0; n < b.N; n++ {
		origMsg.CreateConcatenatedSMS()
	}
}
