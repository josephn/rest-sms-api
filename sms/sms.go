package sms

import (
	"bytes"
	"errors"
	"math/rand"
	"strings"

	"github.com/oscarpfernandez/rest-sms-api/encoding"
)

// "The maximum SMS body length is actually 140 bytes, which equates
// to 160 GSM-encoded characters (7 bits each) or 70 unicode-encoded
// characters (2 bytes each). If you are seeing a maximum message
// length of only 70 characters this is an indication that you are
// requesting unicode encoding."

// According to https://en.wikipedia.org/wiki/Concatenated_SMS
// an User Data Header might have 6 0r 7 bytes

// For this implementation we consider that:
// - the text is GSM encoded.
// - the lenght of the UDH header is 6
// - text payload is 134 bytes (considering GSM7 encoded text,
//   will allow us to pack 153 GSM7 (septets) chars).

const (
	// UDHBytesLen bytes used by the UDH header.
	UDHBytesLen = 6

	// SmsMaxBytes absolute maximum number of bytes in a SMS
	SmsMaxBytes = 140

	// SmsMaxPayloadBytes number of GSM7 encoded chars (septets) allowed.
	SmsMaxPayloadBytes = ((SmsMaxBytes-UDHBytesLen)*8)/7 - 1
)

var (
	ErrEncodingNotSupported = errors.New("The encoding is not supported")
)

// Message  object represents the integral text that should be splitted
// into concatenated SMSs.
type Message struct {
	Originator string
	Recipient  string
	FullText   string
	Encoding   string
}

// SmsBlock object represents one of the parts of a concatenated SMS.
type SmsBlock struct {
	Originator string
	Message    []byte
	UDH        []byte
	Recipients []string
}

// Sms object represents the fully concatenated SMS.
type Sms struct {
	SMSBlocks    []*SmsBlock
	CmsRefNumber byte
}

// CreateConcatenatedSMS given a message utf8 encoded, converts those characters
// to GSM7 encoding (whenever possible) and simulateneously splitting the
// messages as defined by the concatenated SMS protocol, returning an SMS
// object, with all the required parts, so it can be easily processed by
// the Messagebird API.
func (msg *Message) CreateConcatenatedSMS() (*Sms, error) {
	if !strings.EqualFold(msg.Encoding, "utf8") {
		return nil, ErrEncodingNotSupported
	}

	utf8Runes := []rune(msg.FullText)

	sms := &Sms{
		CmsRefNumber: byte(rand.Intn(255)),
	}

	curSmsMaxBytes := 0

	var buffer bytes.Buffer
	buffer.Grow(SmsMaxPayloadBytes)

	for _, utf8Rune := range utf8Runes {
		gsmBytes, len := encoding.ConvertUTF8ToGSM(utf8Rune)

		if len+curSmsMaxBytes < SmsMaxPayloadBytes {
			// Append to buffer and continue
			curSmsMaxBytes += len
			buffer.Write(gsmBytes)
			continue
		} else if len+curSmsMaxBytes == SmsMaxPayloadBytes {
			// Max payload was reached. Save current SMS block.
			curSmsMaxBytes += len
			buffer.Write(gsmBytes)
			sms.saveSmsBlock(buffer.Bytes(), msg.Originator, []string{msg.Recipient})
			curSmsMaxBytes = 0
			buffer.Reset()
			continue
		} else if len+curSmsMaxBytes > SmsMaxPayloadBytes {
			// Save the current SMS block. Initialize a new SMS block,
			// reset the buffer and store the gsm bytes.
			sms.saveSmsBlock(buffer.Bytes(), msg.Originator, []string{msg.Recipient})
			curSmsMaxBytes = 0
			buffer.Reset()
			buffer.Write(gsmBytes)
			curSmsMaxBytes += len
			continue
		}
	}

	if buffer.Len() > 0 {
		// Save the remainder of the buffer
		sms.saveSmsBlock(buffer.Bytes(), msg.Originator, []string{msg.Recipient})
	}

	sms.setUdhInAllSmsBlocks()

	return sms, nil
}

// SaveSmsBlock stores the current buffer content in a SMS blocks
// within the SMS object
func (sms *Sms) saveSmsBlock(buffer []byte, originator string, recipients []string) {
	curSmsBlock := &SmsBlock{
		Message:    make([]byte, len(buffer)),
		Recipients: make([]string, len(recipients)),
		Originator: originator,
	}
	copy(curSmsBlock.Message, buffer)
	copy(curSmsBlock.Recipients, recipients)
	sms.SMSBlocks = append(sms.SMSBlocks, curSmsBlock)
}

// SetUdhInAllSmsBlocks sets the UDH field in all SMS blocks after
// the partition is made. This action must be done after the division
// is concluded since it required to know the number of parts that
// compose the SMS.
func (sms *Sms) setUdhInAllSmsBlocks() {
	totalNumSeqs := byte(len(sms.SMSBlocks))
	for seqID, smsBlock := range sms.SMSBlocks {
		smsBlock.UDH = generateUDH(sms.CmsRefNumber, totalNumSeqs, byte(seqID+1))
	}
}

// generateUDH creates a new UDH byte slice.
func generateUDH(refID byte, numSeqs byte, seqID byte) []byte {
	return []byte{0x05, // Lenght of data header
		0x00,    // Information element identifier
		0x03,    // Length of the header, excluding the first two fields
		refID,   // 00-FF, CSMS reference number, must be same for all the SMS parts in the CSMS
		numSeqs, // 00-FF, total number of parts.
		seqID,   // 01-FF, this part's number in the sequence.
	}
}
